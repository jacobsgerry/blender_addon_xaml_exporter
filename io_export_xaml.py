# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  You should have received a copy of the GNU General Public License
#
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####

import copy
import math
import os
import re
import bpy
import bmesh
from bpy.props import StringProperty, EnumProperty, BoolProperty
from itertools import islice
from mathutils import Quaternion, Vector
from shutil import copyfile

bl_info = {
    "name": "Export Xaml Format (.xaml)",
    "description": "Export to Xaml",
    "author": "Gerry Jacobs",
    "version": (1,4),
    "blender": (2,78,0),
    "location": "File > Export > Xaml",
    "wiki_url": "https://bitbucket.org/jacobsgerry/blender_addon_xaml_exporter/src",
    "warning": "",
    "category": "Import-Export",
}

centerPositions = {}
xamlFile = None

def AreEqual(a,b):
    if type(a) != type(b):
        return False
    if type(a) is float:
        return ("%.5f" % (a)) == ("%.5f" % (b))
    if type(a) is Quaternion:
        return (("%.5f" % (a.w)) == ("%.5f" % (b.w)) and
                ("%.5f" % (a.x)) == ("%.5f" % (b.x)) and
                ("%.5f" % (a.y)) == ("%.5f" % (b.y)) and
                ("%.5f" % (a.z)) == ("%.5f" % (b.z))
               )
    else:
        return a == b
###############################################################################
class XamlType:
    Name = ""
    Owner = None
    Animated = False

    def __init__(self,atom):
        if 'name' not in dir(atom):
            return

        self.Name = self.LegalizeName(atom.name)
        self.Owner = atom

    # remove any punctuation character from a given string, so the resulting
    # string can be used as a name or an identifier of a Xaml element
    def LegalizeName(self,illegalName):
        result = re.sub('^[^A-Za-z_]*', '', illegalName)
        result = re.sub('[^A-Za-z0-9_]', '', result)
        return result

    def RotationQuaternion(self,obj):
        if obj.rotation_mode == 'QUATERNION':
            rotation_quaternion = obj.rotation_quaternion
        elif obj.rotation_mode == 'AXIS_ANGLE':
            rotation_quaternion = Quaternion()
            angle = obj.rotation_axis_angle[0]
            ax = obj.rotation_axis_angle[1]
            ay = obj.rotation_axis_angle[2]
            az = obj.rotation_axis_angle[3]
            rotation_quaternion.x = ax * math.sin(angle/2)
            rotation_quaternion.y = ay * math.sin(angle/2)
            rotation_quaternion.z = az * math.sin(angle/2)
            rotation_quaternion.w = math.cos(angle/2)
        else:
            rotation_quaternion = obj.rotation_euler.to_quaternion()
        return rotation_quaternion

###############################################################################
class AnimationStrip:
    FrameStart = 0
    FrameEnd = 0
    ActionType = []
    XamlCode = []
    Name = ""
    Owner = None
    Repeatable = False

    def __init__(self, owner):
        self.Name = ""
        self.FrameStart = 0
        self.FrameEnd = 0
        self.ActionType = []
        self.XamlCode = []
        self.Owner = owner
        self.Repeatable = False

    def __repr__(self):
        return self.Name
        
###############################################################################
class AnimationNLATrack:
    AnimationStrips = {}
    Element = None
    Type = None
    Subjects = []

    def __init__(self):
        self.AnimationStrips = {}
        self.Element = None
        self.Subjects = []
        self.Type = None

    def __repr__(self):
        ss = "|"
        for x in self.AnimationStrips:
            ss = ss + x + "|"
        return ss

###############################################################################
class AnimationTrack(XamlType):
    NLATracks = []
    Trigger = False
    def __init__(self,animated_element):
        animated_atom = animated_element.Owner
        self.Trigger = False
 
        super().__init__(animated_atom)
        self.NLATracks = []
 
        if 'animation_data' not in dir(animated_atom):
            return  # no animation possible for this element
 
        element_data = None
        if 'data' in dir(animated_atom):
            element_data = animated_atom.data
 
        animation_data = animated_atom.animation_data
        if animation_data == None:
            if element_data != None:
                if 'animation_data' in dir(element_data):
                    animation_data = element_data.animation_data
 
        if animation_data == None:
            if 'parent' in dir(animated_atom):
                if animated_atom.parent != None:
                    parent = animated_atom.parent
                    if 'animation_data' in dir(parent):
                        animation_data = parent.animation_data
                        if animation_data == None:
                            if 'data' in dir(parent):
                                animation_data = parent.data.animation_data
 
        if animation_data != None:
            newNLATrack = None
            # get mesh animation
            if animation_data.nla_tracks:
                for nla_track in animation_data.nla_tracks:
                    newNLATrack = AnimationNLATrack()
                    newNLATrack.Element = animated_element
                    newNLATrack.Type = bpy.types.Mesh
                    for strip in nla_track.strips:
                        self.Trigger = True
                        stripName = self.LegalizeName(strip.name)
                        newStrip = AnimationStrip(strip)
                        newStrip.FrameStart = int(strip.frame_start)
                        newStrip.FrameEnd = int(strip.frame_end)
                        newStrip.Repeatable = (strip.repeat > 1)
                        if newStrip.Repeatable:
                            newStrip.FrameEnd = int(newStrip.FrameStart + (
                                (newStrip.FrameEnd - newStrip.FrameStart) / 
                                strip.repeat))
                        if stripName not in newNLATrack.AnimationStrips:
                            newNLATrack.AnimationStrips[stripName] = newStrip
                    self.NLATracks.append(newNLATrack)
            elif animation_data.action != None:
                # emulate nla track and strip
                newNLATrack = AnimationNLATrack()
                newNLATrack.Element = animated_element
                newNLATrack.Type = bpy.types.Mesh
                stripName = self.LegalizeName(animation_data.action.name)
                newStrip = AnimationStrip(None)
                newStrip.FrameStart = int(bpy.context.scene.frame_start)
                newStrip.FrameEnd = int(bpy.context.scene.frame_end)
                newStrip.Repeatable = True
                if stripName not in newNLATrack.AnimationStrips:
                    newNLATrack.AnimationStrips[stripName] = newStrip
                self.NLATracks.append(newNLATrack)
 
            if newNLATrack != None:
                if animated_atom.type == 'CAMERA':
                    newNLATrack.Type = bpy.types.Camera
                if animated_atom.type == 'LAMP':
                    newNLATrack.Type = bpy.types.Lamp

        for slot in animated_atom.material_slots:
            if slot.material == None:
                continue
 
            if slot.material.animation_data == None:
                continue
 
            # get material animation
            animation_data = slot.material.animation_data
            if animation_data.nla_tracks:
                for nla_track in animation_data.nla_tracks:
                    newNLATrack = AnimationNLATrack()
                    newNLATrack.Element = slot.material
                    newNLATrack.Type = bpy.types.Material
                    for strip in nla_track.strips:
                        self.Trigger = True
                        stripName = self.LegalizeName(strip.name)
                        newStrip = AnimationStrip(strip)
                        newStrip.FrameStart = int(strip.frame_start)
                        newStrip.FrameEnd = int(strip.frame_end)
                        newStrip.Repeatable = (strip.repeat > 1)
                        if newStrip.Repeatable:
                            newStrip.FrameEnd = int(newStrip.FrameStart + (
                                (newStrip.FrameEnd - newStrip.FrameStart) / 
                                strip.repeat))
                        if stripName not in newNLATrack.AnimationStrips:
                            newNLATrack.AnimationStrips[stripName] = newStrip
                    self.NLATracks.append(newNLATrack)
 
    def XamlResource(self):
        fps = bpy.context.scene.render.fps
        for nlaTrack in self.NLATracks:
 
            for stripName in nlaTrack.AnimationStrips:
                keyframes = {}
                strip = nlaTrack.AnimationStrips[stripName]
 
                # initialize
                previous_location = None
                previous_rotation = None
                previous_scaling = None
                previous_visibility = None
                previous_color = None
                initial_location = previous_location
                initial_rotation = previous_rotation
                initial_scaling = previous_scaling
 
                if nlaTrack.Type == bpy.types.Mesh:
                    if not nlaTrack.Element.Positions:
                        continue
                    # process mesh animation
                    object = nlaTrack.Element.Owner
                    object_index = xamlFile.Elements.index(nlaTrack.Element)
                    bpy.context.scene.frame_set(1)
                    object_data = object.matrix_world.copy()
                    object_data = object_data.decompose()
                    initial_location = copy.copy(object_data[0])
                    initial_scaling = copy.copy(object_data[2])
                    object_data = object.matrix_world.copy()
                    object_data.invert()
                    object_data = object_data.decompose()
                    initial_rotation = copy.copy(object_data[1])
                    previous_location = Vector((9999,9999,9999))
                    previous_rotation = Quaternion((9999,9999,9999,9999))
                    previous_scaling = Vector((9999,9999,9999))
                    previous_visibility = not (object.hide or
                                               object.hide_render)

                    for stripFrame in range(strip.FrameStart, strip.FrameEnd + 1):
                        frame = stripFrame
                        bpy.context.scene.frame_set(frame)
 
                        # process element
                        object_data = object.matrix_world.copy()
                        object_data = object_data.decompose()
                        location = object_data[0]
                        scaling = object_data[2]
                        object_data = object.matrix_world.copy()
                        object_data.invert()
                        object_data = object_data.decompose()
                        rotation = object_data[1]
 
                        # get location-x
                        key = "%.5d_%.5d_location_X" % (frame, object_index)
                        value = round(location.x, 5)
                        if not AreEqual(previous_location.x, value):
                            previous_location.x = value
                            keyframes[key] = round(value - initial_location.x, 5)
 
                        # get location-y
                        key = "%.5d_%.5d_location_Y" % (frame, object_index)
                        value = round(location.y, 5)
                        if not AreEqual(previous_location.y, value):
                            previous_location.y = value
                            keyframes[key] = round(value - initial_location.y, 5)
 
                        # get location-z
                        key = "%.5d_%.5d_location_Z" % (frame, object_index)
                        value = round(location.z, 5)
                        if not AreEqual(previous_location.z, value):
                            previous_location.z = value
                            keyframes[key] = round(value - initial_location.z, 5)
 
                        # get rotation
                        if not AreEqual(previous_rotation, rotation):
                            previous_rotation = rotation
                            #center = nlaTrack.Element.CenterPosition()
                            center = Vector()
                            center.x = location.z
                            center.y = location.y
                            center.z = location.x
 
                            key = "%.5d_%.5d_rotation_X" % (frame, object_index)
                            value = round(center.x, 5)
                            keyframes[key] = value
 
                            key = "%.5d_%.5d_rotation_Y" % (frame, object_index)
                            value = round(center.y, 5)
                            keyframes[key] = value
 
                            key = "%.5d_%.5d_rotation_Z" % (frame, object_index)
                            value = round(center.z, 5)
                            keyframes[key] = value
 
                            key = "%.5d_%.5d_rotation_Angle" % (frame, object_index)
                            value = "%s,%s,%s,%s" % (
                                round(rotation.x - initial_rotation.x, 5),
                                round(rotation.y - initial_rotation.y, 5),
                                round(rotation.z - initial_rotation.z, 5),
                                round(rotation.w, 5))
                            keyframes[key] = value
 
                        # get scaling-x
                        key = "%.5d_%.5d_scaling_X" % (frame, object_index)
                        value = round(scaling.x, 5)
                        if not AreEqual(previous_scaling.x, value):
                            previous_scaling.x = value
                            keyframes[key] = round(value / initial_scaling.x, 5)
 
                        # get scaling-y
                        key = "%.5d_%.5d_scaling_Y" % (frame, object_index)
                        value = round(scaling.y, 5)
                        if not AreEqual(previous_scaling.y, value):
                            previous_scaling.y = value
                            keyframes[key] = round(value / initial_scaling.y, 5)
 
                        # get scaling-z
                        key = "%.5d_%.5d_scaling_Z" % (frame, object_index)
                        value = round(scaling.z, 5)
                        if not AreEqual(previous_scaling.z, value):
                            previous_scaling.z = value
                            keyframes[key] = round(value / initial_scaling.z, 5)
 
                        # get visibility
                        key = "%.5d_%.5d_visibility_" % (frame, object_index)
                        show = not (object.hide or
                                    object.hide_render)
                        if not AreEqual(previous_visibility, show):
                            nlaTrack.Element.CanBeHidden = True
                            previous_visibility = show
                            if show:
                                internalValue = 1
                            else:
                                internalValue = 0
                            keyframes[key + "X"] = internalValue
                            keyframes[key + "Y"] = internalValue
                            keyframes[key + "Z"] = internalValue
 
                elif nlaTrack.Type == bpy.types.Material:
                    # process material animation
                    material = nlaTrack.Element
                    materialName = self.LegalizeName(material.name)
                    materialElement = list(m for m in xamlFile.Materials 
                        if m.Name == materialName)[0]

                    # collect the subjects which consist of this material
                    for obj in bpy.data.objects:
                        object_index = 0
                        while object_index < len(xamlFile.Elements):
                            if xamlFile.Elements[object_index].Owner == obj:
                                xamlFile.Elements[object_index].ObjectIndex = object_index
                                nlaTrack.Subjects.append(xamlFile.Elements[object_index])
                                break
                            object_index += 1
                                    
                    for stripFrame in range(strip.FrameStart + 1, strip.FrameEnd + 1):
                        frame = stripFrame
                        bpy.context.scene.frame_set(frame)
 
                        #get diffuse_color + alpha
                        if material.diffuse_color != None:
                            alpha = material.alpha
                            if alpha == None:
                                alpha = 1
                            alpha = int(alpha * 255)
                            r = int(material.diffuse_color.r * 255)
                            g = int(material.diffuse_color.g * 255)
                            b = int(material.diffuse_color.b * 255)
                            color = "#%.2x%.2x%.2x%.2x" % (alpha,r,g,b)
                            for subject in nlaTrack.Subjects:
                                slot_index = -1
                                for slot in subject.Owner.material_slots:
                                    slot_index += 1
                                    if slot.material == material:
                                        key = "%.5d_%.5d_color_0" % (
                                                frame, subject.ObjectIndex
                                                )
                                        keyframes[key] = color

                            if material.emit > 0:
                                materialElement.Emit = True
                                alpha = int(255 * (material.emit / 1.5))
                                if alpha > 255:
                                    alpha = 255
                                color = "#%.2x%.2x%.2x%.2x" % (alpha,r,g,b)
                                for subject in nlaTrack.Subjects:
                                    slot_index = -1
                                    for slot in subject.Owner.material_slots:
                                        slot_index += 1
                                        if slot.material == material:
                                            key = "%.5d_%.5d_emit_2" % (
                                                    frame, subject.ObjectIndex
                                                    )
                                            keyframes[key] = color

                elif nlaTrack.Type == bpy.types.Camera:
                    object = nlaTrack.Element.Owner
                    object_index = 0  # N/A
                    previous_cameralocation = "0,0,0"
                    previous_rotation = Quaternion()

                    for stripFrame in range(strip.FrameStart, strip.FrameEnd + 1):
                        frame = stripFrame
                        bpy.context.scene.frame_set(frame)
 
                        # process element
                        object_data = object.matrix_world.decompose()
                        location = object_data[0]
                        rotation = object_data[1]

                        # get position
                        key = "%.5d_%.5d_cameralocation_1" % (frame, object_index)
                        value = "%s,%s,%s" % (
                            round(location.x, 5), 
                            round(location.y, 5), 
                            round(location.z, 5))

                        if (not AreEqual(previous_cameralocation, value) or
                            not AreEqual(previous_rotation, rotation)):
                            xamlFile.UseFixedCameraViews = True
                            previous_cameralocation = value
                            previous_rotation = rotation

                            keyframes[key] = value;

                            # replace rotation-center-position also
                            key = "%.5d_%.5d_cameralocation_X" % (frame, object_index)
                            keyframes[key] = round(location.x, 5);
                            key = "%.5d_%.5d_cameralocation_Y" % (frame, object_index)
                            keyframes[key] = round(location.y, 5);
                            key = "%.5d_%.5d_cameralocation_Z" % (frame, object_index)
                            keyframes[key] = round(location.z, 5);
 
                            key = "%.5d_%.5d_camerarotation_1" % (frame, object_index)
                            value = "%s,%s,%s,%s" % (
                                round(rotation.x, 5), 
                                round(rotation.y, 5), 
                                round(rotation.z, 5), 
                                round(rotation.w, 5))
                            keyframes[key] = value

                elif nlaTrack.Type == bpy.types.Lamp:
                    object = nlaTrack.Element.Owner
                    object_index = (len(xamlFile.Elements) + 
                                    xamlFile.Lights.index(nlaTrack.Element))
                    bpy.context.scene.frame_set(1)
                    object_data = object.matrix_world.decompose()
                    previous_location = copy.copy(object_data[0])
                    initial_location = copy.copy(object_data[0])

                    for stripFrame in range(strip.FrameStart, strip.FrameEnd + 1):
                        frame = stripFrame
                        bpy.context.scene.frame_set(frame)
 
                        # process element
                        object_data = object.matrix_world.decompose()
                        location = object_data[0]
                        rotation = object_data[1]

                        # get location-x
                        key = "%.5d_%.5d_location_X" % (frame, object_index)
                        value = round(location.x, 5)
                        if not AreEqual(previous_location.x, value):
                            previous_location.x = value
                            keyframes[key] = round(value - initial_location.x, 5)
 
                        # get location-y
                        key = "%.5d_%.5d_location_Y" % (frame, object_index)
                        value = round(location.y, 5)
                        if not AreEqual(previous_location.y, value):
                            previous_location.y = value
                            keyframes[key] = round(value - initial_location.y, 5)
 
                        # get location-z
                        key = "%.5d_%.5d_location_Z" % (frame, object_index)
                        value = round(location.z, 5)
                        if not AreEqual(previous_location.z, value):
                            previous_location.z = value
                            keyframes[key] = round(value - initial_location.z, 5)

                filtered_keyframes = {}
                for key in keyframes:
                    if len(list(k for k in list(keyframes) if k.startswith(key[6:], 6))) > 1:
                        filtered_keyframes[key] = keyframes[key]
                    elif "_color_" in key:
                        filtered_keyframes[key] = keyframes[key]
 
                keyframes = filtered_keyframes
 
                # if no keyframes were updated, go to the next strip/track
                if not keyframes:
                    continue
 
                previous_frame = int(min(keyframes).split('_')[0])
 
                keyframeActions = set(k.split('_')[1] + k.split('_')[2] + k.split('_')[3] for k in keyframes)
                actionInit = {}
                for keyframeAction in keyframeActions:
                    actionInit[keyframeAction] = True;
                
                frame = int(sorted(keyframes)[0].split('_')[0])
                for keyframe in sorted(keyframes):
                    key_elements = keyframe.split('_')
                    if frame != int(key_elements[0]):
                        previous_frame = frame
                    frame = int(key_elements[0])
                    object_index = int(key_elements[1])
                    parameter = key_elements[2]
                    subparameter = key_elements[3]
                    action = key_elements[1] + parameter + subparameter
 
                    duration_seconds = (frame - previous_frame) / fps
                    duration_hours = int(duration_seconds / 3600)
                    duration_minutes = int((duration_seconds - duration_hours * 3600) / 60)
                    duration_seconds = round(duration_seconds % 60, 3)
                    duration = "%s:%s:%s" % (duration_hours, duration_minutes, duration_seconds )
                    if actionInit[action]:
                        begin_time_seconds = 0
                        actionInit[action] = False
                    else:
                        if self.Trigger:
                            end_frame = frame - strip.FrameStart
                        else:
                            end_frame = frame
                        end_time_seconds = round(end_frame / fps, 3)
                        begin_time_seconds = .0 + end_time_seconds - duration_seconds

                    begin_time_hours = int(begin_time_seconds / 3600)
                    begin_time_minutes = int((begin_time_seconds - begin_time_hours * 3600) / 60)
                    begin_time_seconds = round(begin_time_seconds % 60, 3)
                    begin_time = "%s:%s:%s" % (begin_time_hours, begin_time_minutes, begin_time_seconds )
 
                    if parameter == "color":
                        target_property = """<ColorAnimation Storyboard.TargetProperty=\""""
                        target_property += """Children[0].Children[%s].""" % (object_index)
                        target_property += """Children[0].Content.Material."""
                        target_property += """Children[%s].Brush.Color""" % (subparameter)
                    elif parameter == "emit":
                        target_property = """<ColorAnimation Storyboard.TargetProperty=\""""
                        target_property += """Children[0].Children[%s].""" % (object_index)
                        target_property += """Children[0].Content.Material."""
                        target_property += """Children[%s].Color""" % (subparameter)
                    elif parameter == "cameralocation":
                        if subparameter == "1":
                            target_property = """<Point3DAnimation Storyboard.TargetProperty=\""""
                            target_property += """Camera.Position"""
                        else:
                            target_property = """<DoubleAnimation Storyboard.TargetProperty=\""""
                            target_property += """Camera.Transform.Center%s""" % (subparameter)
                    elif parameter == "camerarotation":
                        target_property = """<QuaternionAnimation Storyboard.TargetProperty=\""""
                        target_property += """Camera.Transform.Rotation.Quaternion"""
                    else:
                        target_property_header = """<DoubleAnimation Storyboard.TargetProperty=\""""
                        target_property_header += """Children[0].Children[%s]""" % (object_index)
 
                        if parameter == "location":
                            target_property_header += """.Transform.Children"""
                            target_property = "%s[0].Offset%s" % (
                                    target_property_header, subparameter)
                        elif parameter == "rotation":
                            if subparameter == "Angle":
                                target_property_header = """<QuaternionAnimation Storyboard.TargetProperty=\""""
                                target_property_header += """Children[0].Children[%s]""" % (object_index)
                                target_property_header += """.Transform.Children"""
                                target_property = "%s[1].Rotation.Quaternion" % (
                                        target_property_header)
                            else:
                                target_property_header += """.Transform.Children"""
                                target_property = "%s[1].Center%s" % (
                                        target_property_header, subparameter)
                        elif parameter == "scaling":
                            target_property_header += """.Transform.Children"""
                            target_property = "%s[2].Scale%s" % (
                                    target_property_header, subparameter)
                        elif parameter == "visibility":
                            target_property_header += """.Children[0]"""
                            target_property_header += """.Transform"""
                            target_property = "%s.Scale%s" % (
                                    target_property_header, subparameter)
                        else:
                            continue
 
                    target_property += """" To="%s" BeginTime="%s" Duration="%s" />""" % (
                                        keyframes[keyframe], begin_time, duration)

                    target_property = """
                    %s""" % (target_property)
 
                    # in case we have an transparent color, we'll change the
                    # ScaleXYZ of the object, because a transparent object
                    # is still visible in WPF/XAML
                    if parameter == "color":
                        nbrObjectActions = len(set(k.split('_')[1] + '_' + k.split('_')[2] for k in keyframes))
                        nbrActions = len(set(k[6:] for k in keyframes))
                        if nbrObjectActions == nbrActions:
                            for subject in nlaTrack.Subjects:
                                subject.CanBeHidden = True
                            if keyframes[keyframe][1:3] == "00":
                                scaleValue = 0
                            else:
                                scaleValue = 1
                            for scaleName in ['X', 'Y', 'Z']:
                                target_property += """
                    <DoubleAnimation Storyboard.TargetProperty="Children[0].Children[%s].Children[0].Transform.Scale%s" To="%s" BeginTime="0:0:0" Duration="0:0:0.0" />""" % (
                                object_index, scaleName, scaleValue)
                            
                    strip = nlaTrack.AnimationStrips[stripName]
                    actionType = str(object_index) + parameter + subparameter
                    if actionType not in strip.ActionType:
                        strip.ActionType.append(actionType)
                    strip.XamlCode.append(target_property)
                    strip.Name = stripName

###############################################################################
class XamlCamera(XamlType):
    Fake = False
    FieldOfView = 45.0
    Position = Vector()
    Rotation = Quaternion()

    def __init__(self,camera):
        if camera == None:
            self.Fake = True
            self.Name = "FakeCamera"
        else:
            super().__init__(camera)
            self.FieldOfView = round(camera.data.angle * 180 / math.pi, 3)
            self.Position.x = round(camera.location.x, 3)
            self.Position.y = round(camera.location.y, 3)
            self.Position.z = round(camera.location.z, 3)
            rotation_quaternion = super().RotationQuaternion(camera)
            self.Rotation.w = rotation_quaternion.w
            self.Rotation.x = rotation_quaternion.x
            self.Rotation.y = rotation_quaternion.y
            self.Rotation.z = rotation_quaternion.z

###############################################################################
class XamlMaterial(XamlType):
    MaterialElements = []
    DiffuseMaterialSolidColor = ""
    DiffuseMaterialImageSource = ""
    SpecularPower = ""
    SpecularColor = ""
    Emit = 0.0

    def __init__(self,material = None):
        super().__init__(material)

        self.MaterialElements = []

        if material == None:
            return

        if material.use_transparency == True:
            diffuse_alpha = int(material.alpha * 255)
        else:
            diffuse_alpha = 255

        self.DiffuseMaterialSolidColor = "#%.2x%.2x%.2x%.2x" % (diffuse_alpha,
                               int(material.diffuse_color.r * 255),
                               int(material.diffuse_color.g * 255),
                               int(material.diffuse_color.b * 255
                               ))

        self.SpecularColor = "#%.2x%.2x%.2x%.2x" % (int(material.specular_alpha * 255),
                               int(material.specular_color.r * 255),
                               int(material.specular_color.g * 255),
                               int(material.specular_color.b * 255
                               ))

        # calculate light reflectance
        self.SpecularPower = 3 + abs(math.log10(material.specular_intensity + 0.001)) * 100
        if material.specular_shader == 'COOKTORR':
            self.SpecularPower *= (material.specular_hardness + 1) / 256
        elif material.specular_shader == 'WARDISO':
            self.SpecularPower *= round(10 + abs(material.specular_slope - 0.2) * 150, 3)
        elif material.specular_shader == 'TOON':
            self.SpecularPower *= (1.530 - material.specular_toon_size)
        elif material.specular_shader == 'BLINN':
            self.SpecularPower *= (material.specular_hardness + 1) / 32
        elif material.specular_shader == 'PHONG':
            self.SpecularPower *= (material.specular_hardness + 1) / 32

        self.SpecularPower = round(self.SpecularPower)
        self.Emit = round(material.emit)

###############################################################################
class XamlLight(XamlType):
    Type = None
    Color = ""
    LinearAttenuation = None
    Position = Vector()
    Energy = None

    def __init__(self,light):
        super().__init__(light)

        if light != None:
            self.Position = light.location
            
            useBlenderRender = True
            if light.data.node_tree != None:
                if 'Emission' in light.data.node_tree.nodes:
                    useBlenderRender = False
                    
            if useBlenderRender:
                # Blender Render
                self.Color = "#%.2x%.2x%.2x" % (
                        int(light.data.color.r * 255),
                        int(light.data.color.g * 255),
                        int(light.data.color.b * 255
                        ))
    
                if light.data.type == "POINT":
                    self.Type = "PointLight"
                    self.LinearAttenuation = 6 * (1 - (light.data.energy % 100) / 100) / 10
                    self.Energy = min(100, light.data.energy) % 101
                elif light.data.type == "SUN":
                    self.Type = "DirectionalLight"
                elif light.data.type == "SPOT":
                    self.Type = "PointLight"
                    self.LinearAttenuation = (1 - (light.data.energy % 100) / 100) / 10
                    self.Energy = min(100, light.data.energy) % 101
                elif light.data.type == "HEMI":
                    self.Type = "AmbientLight"
                elif light.data.type == "AREA":
                    self.Type = "PointLight"
                    self.LinearAttenuation = 0
                    self.Energy = 20
            else:
                # Cycles Render
                node = light.data.node_tree.nodes['Emission'].inputs
                self.Color = "#%.2x%.2x%.2x" % (
                    int(node['Color'].default_value[0] * 255),
                    int(node['Color'].default_value[1] * 255),
                    int(node['Color'].default_value[2] * 255)
                    )
                    
                if light.data.type == "POINT":
                    self.Type = "PointLight"
                    self.LinearAttenuation = 0.5
                    self.Energy = min(10000, node['Strength'].default_value) % 10001
                    self.Energy = self.Energy / 100
                elif light.data.type == "SUN":
                    self.Type = "DirectionalLight"
                elif light.data.type == "SPOT":
                    self.Type = "PointLight"
                    self.LinearAttenuation = 0.5
                    self.Energy = min(10000, node['Strength'].default_value) % 10001
                    self.Energy = self.Energy / 100
                elif light.data.type == "HEMI":
                    self.Type = "AmbientLight"
                elif light.data.type == "AREA":
                    self.Type = "PointLight"
                    self.LinearAttenuation = 0
                    self.Energy = 20

###############################################################################
class XamlElement(XamlType):
    Material = {}
    BackMaterial = {}
    Positions = {}
    TriangleIndices = {}
    Normals = {}
    TextureCoordinates = {}
    Center = Vector()   # necessary for animation stuff
    CanBeHidden = False
    Owner = None
    ObjectIndex = 0

    def __repr__(self):
        return self.Name

    def __init__(self,obj):
        super().__init__(obj)

        global xamlFile

        self.Material = {}
        self.BackMaterial = {}
        self.Positions = {}
        self.TriangleIndices = {}
        self.Normals = {}
        self.TextureCoordinates = {}
        self.Center = Vector()
        self.CanBeHidden = False
        self.ObjectIndex = 0

        mesh = obj.to_mesh(bpy.context.scene, True, "PREVIEW")
        if len(mesh.tessfaces) == 0:
            return  # no faces to draw

        old_rotation_value = None
        try:
            # in case an object has an angle of 90 degrees, the resulting Xaml
            # object might be shown differently.
            # we'll change the object's angle temporarily to 90.1 degrees
            obj_rotation = self.RotationQuaternion(obj).to_euler('XYZ')
            if round(obj_rotation.y, 5) == round(math.pi / 2):
                old_rotation_mode = obj.rotation_mode
                obj_rotation.y = 90.1 * math.pi / 180
                obj.rotation_mode = "XYZ"
                old_rotation_value = obj.rotation_euler
                obj.rotation_euler = obj_rotation

            # initialize faces indexer
            face_index = 0

            # loop through all material parts
            while face_index < len(mesh.tessfaces):
                # check if object has a UV texture
                uv_texture_slots = list(uv_texture_slot for uv_texture_slot in mesh.uv_textures)
                if len(uv_texture_slots) > 0:
                    uv_texture = uv_texture_slots[0].data[0]
                else:
                    uv_texture = None

                material_index = mesh.tessfaces[face_index].material_index
                if len(mesh.materials) == 0 or mesh.materials[material_index] == None:
                    if uv_texture == None:
                        material_name = self.LegalizeName("SomeMaterial")
                        xamlFile.CreateSomeMaterial = True
                    else:
                        material_name = self.LegalizeName(uv_texture_slots[0].name)
                else:
                    material_name = self.LegalizeName(mesh.materials[material_index].name)

                # determine subelement indexer
                subElement = len(self.Positions)
                self.Material[subElement] = None
                self.BackMaterial[subElement] = None

                # determine material
                self.Material[subElement] = XamlMaterial()
                self.Material[subElement].Name = material_name
                if obj.type == "MESH":
                    self.BackMaterial[subElement] = self.Material[subElement]

                # determine faces' positions, triangles, normals and UV's
                self.Positions[subElement] = ""
                self.TriangleIndices[subElement] = ""
                self.Normals[subElement] = ""
                self.TextureCoordinates[subElement] = ""

                uv_face_vertices = []
                vertex_in_use = []
                while face_index < len(mesh.tessfaces):
                    face = mesh.tessfaces[face_index]
                    if material_index != face.material_index:
                        break

                    material_index = face.material_index

                    if len(face.vertices) == 4:
                        self.TriangleIndices[subElement] += str(len(uv_face_vertices) + 0) + " "
                        self.TriangleIndices[subElement] += str(len(uv_face_vertices) + 1) + " "
                        self.TriangleIndices[subElement] += str(len(uv_face_vertices) + 3) + " "

                        self.TriangleIndices[subElement] += str(len(uv_face_vertices) + 1) + " "
                        self.TriangleIndices[subElement] += str(len(uv_face_vertices) + 2) + " "
                        self.TriangleIndices[subElement] += str(len(uv_face_vertices) + 3) + " "
                    elif len(face.vertices) == 3:
                        self.TriangleIndices[subElement] += str(len(uv_face_vertices) + 0) + " "
                        self.TriangleIndices[subElement] += str(len(uv_face_vertices) + 1) + " "
                        self.TriangleIndices[subElement] += str(len(uv_face_vertices) + 2) + " "
                    elif len(face.vertices) > 4:
                        for sub_triangle in range(0, len(face.vertices) - 1):
                            self.TriangleIndices[subElement] += str(len(uv_face_vertices)) + " "
                            self.TriangleIndices[subElement] += str(len(uv_face_vertices) + sub_triangle) + " "
                            self.TriangleIndices[subElement] += str(len(uv_face_vertices) + sub_triangle + 1) + " "


                    if uv_texture != None:
                        for vertice_index in range(0, len(face.vertices)):
                            vertice = mesh.vertices[face.vertices[vertice_index]]
                            vertice_coordinates = obj.matrix_world * vertice.co
                            uv_face_vertices.append(vertice_coordinates)

                            normal1 = round(vertice.normal[0], 3)
                            normal2 = round(vertice.normal[1], 3)
                            normal3 = round(vertice.normal[2], 3)
                            if math.isnan(normal3):
                                normal3 = (normal1 + normal2) / 2

                            self.Normals[subElement] += "%g,%g,%g " % (
                                        normal1, normal2, normal3
                                        )
                    else:
                        for vertice_index in range(0, len(face.vertices)):
                            vertice = mesh.vertices[face.vertices[vertice_index]]
                            vertice_coordinates = obj.matrix_world * vertice.co
                            uv_face_vertices.append(vertice_coordinates)

                            normal1 = round(face.normal[0], 3)
                            normal2 = round(face.normal[1], 3)
                            normal3 = round(face.normal[2], 3)
                            
                            # remove useless sign character when 0
                            if normal1 == -0:
                                normal1 = 0
                            if normal2 == -0:
                                normal2 = 0
                            if normal3 == -0:
                                normal3 = 0
                                
                            if math.isnan(normal3):
                                normal3 = (normal1 + normal2) / 2

                            self.Normals[subElement] += "%g,%g,%g " % (
                                        normal1, normal2, normal3
                                        )

                    face_index = face_index + 1

                for v in uv_face_vertices:
                    self.Positions[subElement] += "%g,%g,%g " % (
                                round(v[0], 3), round(v[1], 3), round(v[2], 3)
                                )

                if uv_texture != None:
                    for uv_texture_data in mesh.tessface_uv_textures.active.data:
                        for uv in uv_texture_data.uv:
                            self.TextureCoordinates[subElement] += "%g,%g " % (
                                        round(uv[0], 3), -round(uv[1], 3)
                                        )
        finally:
            # correct the object's angle again if we changed it before
            if old_rotation_value != None:
                obj.rotation_euler = old_rotation_value
                obj.rotation_mode = old_rotation_mode

    def CenterPosition(self):
        global centerPositions
        if self.Owner not in centerPositions:
            min_x = min(self.Owner.bound_box[0][0], self.Owner.bound_box[1][0],
                        self.Owner.bound_box[2][0], self.Owner.bound_box[3][0],
                        self.Owner.bound_box[4][0], self.Owner.bound_box[5][0],
                        self.Owner.bound_box[6][0], self.Owner.bound_box[7][0])
            max_x = max(self.Owner.bound_box[0][0], self.Owner.bound_box[1][0],
                        self.Owner.bound_box[2][0], self.Owner.bound_box[3][0],
                        self.Owner.bound_box[4][0], self.Owner.bound_box[5][0],
                        self.Owner.bound_box[6][0], self.Owner.bound_box[7][0])
            min_y = min(self.Owner.bound_box[0][1], self.Owner.bound_box[1][1],
                        self.Owner.bound_box[2][1], self.Owner.bound_box[3][1],
                        self.Owner.bound_box[4][1], self.Owner.bound_box[5][1],
                        self.Owner.bound_box[6][1], self.Owner.bound_box[7][1])
            max_y = max(self.Owner.bound_box[0][1], self.Owner.bound_box[1][1],
                        self.Owner.bound_box[2][1], self.Owner.bound_box[3][1],
                        self.Owner.bound_box[4][1], self.Owner.bound_box[5][1],
                        self.Owner.bound_box[6][1], self.Owner.bound_box[7][1])
            min_z = min(self.Owner.bound_box[0][2], self.Owner.bound_box[1][2],
                        self.Owner.bound_box[2][2], self.Owner.bound_box[3][2],
                        self.Owner.bound_box[4][2], self.Owner.bound_box[5][2],
                        self.Owner.bound_box[6][2], self.Owner.bound_box[7][2])
            max_z = max(self.Owner.bound_box[0][2], self.Owner.bound_box[1][2],
                        self.Owner.bound_box[2][2], self.Owner.bound_box[3][2],
                        self.Owner.bound_box[4][2], self.Owner.bound_box[5][2],
                        self.Owner.bound_box[6][2], self.Owner.bound_box[7][2])
            location = self.Owner.matrix_world.decompose()[0]

            result = Vector()
            result.x = location.x + min_x + (max_x - min_x) / 2
            result.y = location.y + min_y + (max_y - min_y) / 2
            result.z = location.z + min_z + (max_z - min_z) / 2

            centerPositions[self.Owner] = result

        return centerPositions[self.Owner]

###############################################################################
class XamlFile:
    __outputpath = None
    __csfilepath = None
    __xamlfilepath = None
    __usePropertyTriggers = False

    ActiveCamera = None
    CreateSomeMaterial = False
    UseFixedCameraViews = False
    
    AnimationTracks = {}

    AnimationStrips = {}
    Cameras = []
    Elements = []
    Lights = []
    Materials = []

    def __init__(self,filepath, usePropertyTriggers):
        self.CreateSomeMaterial = False
        self.__usePropertyTriggers = usePropertyTriggers

        self.AnimationTracks = {}
        self.AnimationStrips = {}
        self.Cameras = []
        self.Elements = []
        self.Lights = []
        self.Materials = []

        global xamlFile
        xamlFile = self

        if filepath == "":
            return

        filepath_parts = filepath.replace('\\','/').split('/')
        self.__outputpath = "/".join(islice(filepath_parts, 
                                            len(filepath_parts) - 1))
        if filepath[-5:].upper() == ".XAML":
            self.__xamlfilepath = filepath
            self.__csfilepath = filepath[:-5] + ".cs"
        else:
            self.__xamlfilepath = filepath + ".xaml"
            self.__csfilepath = filepath + ".cs"

        # change work-mode to object-mode
        bpy.ops.object.mode_set(mode='OBJECT', toggle=False)

        # prepare animation stuff
        bpy.context.scene.frame_set(1)
        if self.__usePropertyTriggers:
            # try converting actions to nla tracks/strips
            for obj in bpy.data.objects:
                if obj.animation_data != None:
                    action = obj.animation_data.action
                    if action != None:
                        nla_track = obj.animation_data.nla_tracks.new()
                        nla_track.strips.new(action.name,
                                             action.frame_range[0],
                                             action)
                        obj.animation_data.action = None

        # collect materials + their animations
        for atom in bpy.data.materials:
            newMaterial = XamlMaterial(atom)
            for obj in bpy.data.objects:
                for slot in obj.material_slots:
                    if slot.material == atom:
                        newMaterial.MaterialElements.append(obj)
            self.Materials.append(newMaterial)

        # collect unique UV map names and convert it into a material
        uvnames = set(uv.name for m in bpy.data.meshes for uv in m.uv_textures)
        for uvname in uvnames:
            newMaterial = XamlMaterial()
            newMaterial.Name = uvname
            uv_meshes = list(m for m in bpy.data.meshes
                    for uv in m.uv_textures if uv.name == uvname)
            if uv_meshes:
                imageFile = uv_meshes[0].uv_textures[0].data[0].image.filepath_from_user()
                newMaterial.DiffuseMaterialImageSource = imageFile
                self.Materials.append(newMaterial)

        # collect atoms
        for obj in bpy.context.scene.objects:
            newAnimation = None
            if obj.type == 'MESH':
                newAtom = XamlElement(obj)
                newAnimation = AnimationTrack(newAtom)
                if newAtom.Positions:
                    self.Elements.append(newAtom)
            elif obj.type == 'CURVE':
                newAtom = XamlElement(obj)
                newAnimation = AnimationTrack(newAtom)
                if newAtom.Positions:
                    self.Elements.append(newAtom)
            elif obj.type == 'LAMP':
                newAtom = XamlLight(obj)
                newAnimation = AnimationTrack(newAtom)
                self.Lights.append(newAtom)
            elif obj.type == 'FONT':
                newAtom = XamlElement(obj)
                newAnimation = AnimationTrack(newAtom)
                if newAtom.Positions:
                    self.Elements.append(newAtom)
            elif obj.type == 'CAMERA':
                newAtom = XamlCamera(obj)
                newAnimation = AnimationTrack(newAtom)
                self.Cameras.append(newAtom)
                if bpy.context.scene.camera == obj:
                    self.ActiveCamera = newAtom

            # collect animation if any
            if newAnimation != None:
                if newAnimation.Name not in self.AnimationTracks:
                    self.AnimationTracks[newAnimation.Name] = newAnimation
                if not self.__usePropertyTriggers:
                    newAnimation.Trigger = False
                newNLATrack = AnimationNLATrack()
                newNLATrack.Element = newAtom
                self.AnimationTracks[newAnimation.Name].NLATracks.append(newNLATrack)
                newAtom.Animated = True
                newAtom.Owner = obj

        if self.ActiveCamera == None:
            self.ActiveCamera = XamlCamera(None)

        if len(self.Lights) == 0:
            # create a directional light with the source pointing from
            # somewhere near the camera position
            generatedLight = XamlLight(None)
            generatedLight.Name = "EnvironmentalLight"
            generatedLight.Type = "DirectionalLight"
            generatedLight.Color = "#e0ffff"
            generatedLight.Position = Vector()
            generatedLight.Position.x = -self.ActiveCamera.Position.x + 5
            generatedLight.Position.y = -self.ActiveCamera.Position.y + 5
            generatedLight.Position.z = -self.ActiveCamera.Position.z + 5
            self.Lights.append(generatedLight)
            
    def ConcurrentAnimationStrips(self, strips, otherStrips):
        for strip in strips.ActionType:
            for otherStrip in otherStrips.ActionType:
                if strip == otherStrip:
                    return True
        return False

    def WriteXaml(self):
        file = None
        try:
            file = open(self.__xamlfilepath, 'w')

            file.write("""<?xml version="1.0" encoding="utf-8"?>
<Page xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation" xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"  Width="500" Height="500">
    <!-- Begin Blender content -->
    <Border Background="{DynamicResource {x:Static SystemColors.ControlBrushKey}}">
        <Viewport3D>
            <Viewport3D.Resources>""")

            # collect all generated animation xaml code and store it in a
            # dictionary with sorted values based on BeginTime
            storyboardAnimations = {}
            storyboardRepeat = {}
            for animationName in sorted(self.AnimationTracks):
                self.AnimationTracks[animationName].XamlResource()
                for nla in self.AnimationTracks[animationName].NLATracks:
                    for stripName in nla.AnimationStrips:
                        if self.__usePropertyTriggers:
                            storyboardName = "storyboard" + stripName
                        else:
                            storyboardName = "MyStoryboard"
                        if storyboardName not in storyboardAnimations:
                            storyboardAnimations[storyboardName] = []
                        strip = nla.AnimationStrips[stripName]
                        if storyboardName not in storyboardRepeat:
                            storyboardRepeat[storyboardName] = strip.Repeatable
                        elif storyboardRepeat[storyboardName] == False:
                            storyboardRepeat[storyboardName] = strip.Repeatable
                        for xamlCode in strip.XamlCode:
                            beginTime1 = re.sub(".*\" BeginTime=\"([^\"]*)\".*", r"\1", xamlCode, 0, re.DOTALL)
                            index = 0
                            while True:
                                if index >= len(storyboardAnimations[storyboardName]):
                                    break
                                xamlCode2 = storyboardAnimations[storyboardName][index]
                                beginTime2 = re.sub(".*\" BeginTime=\"([^\"]*)\".*", r"\1", xamlCode2, 0, re.DOTALL)
                                if beginTime1 < beginTime2:
                                    break
                                index += 1
                            storyboardAnimations[storyboardName].insert(index, xamlCode)

            for storyboardName in sorted(storyboardAnimations):
                repeat = ""
                if storyboardRepeat[storyboardName]:
                    repeat = """ RepeatBehavior="Forever\""""
                file.write("""
                <Storyboard x:Key="%s"%s>""" % (
                    storyboardName, repeat))
                for animation in storyboardAnimations[storyboardName]:
                    file.write(animation)
                file.write("""
                </Storyboard>""")

            # write cameras
            for camera in self.Cameras:
                if camera.Fake:
                    file.write("""
                <PerspectiveCamera x:Key="Camera"
                    UpDirection="0 1 0"
                    Position="0 0 0"
                    LookDirection="1 1 1">
                </PerspectiveCamera>""")
                else:
                    file.write("""
                <PerspectiveCamera x:Key="%s"
                                FieldOfView="%s"
                                UpDirection="0 1 0"
                                Position="%s %s %s">
                    <PerspectiveCamera.Transform>
                        <RotateTransform3D CenterX="%s" CenterY="%s" CenterZ="%s">
                            <RotateTransform3D.Rotation>
                                <QuaternionRotation3D>
                                    <QuaternionRotation3D.Quaternion>
                                        <Quaternion W="%s" X="%s" Y="%s" Z="%s" />
                                    </QuaternionRotation3D.Quaternion>
                                </QuaternionRotation3D>
                            </RotateTransform3D.Rotation>
                        </RotateTransform3D>
                    </PerspectiveCamera.Transform>
                </PerspectiveCamera>""" %
                (camera.Name,
                 camera.FieldOfView,
                 round(camera.Position.x, 5), round(camera.Position.y, 5),
                 round(camera.Position.z, 5),
                 round(camera.Position.x, 5), round(camera.Position.y, 5),
                 round(camera.Position.z, 5),
                 round(camera.Rotation.w, 5), round(camera.Rotation.x, 5),
                 round(camera.Rotation.y, 5), round(camera.Rotation.z, 5),
                ))

            # write materials
            for material in self.Materials:
                file.write("""
                <MaterialGroup x:Key="%s">""" % (
                material.Name
                ))

                if material.DiffuseMaterialSolidColor != "":
                    file.write("""
                    <DiffuseMaterial>
                        <DiffuseMaterial.Brush>
                            <SolidColorBrush Color="%s"/>
                        </DiffuseMaterial.Brush>
                    </DiffuseMaterial>""" % (
                material.DiffuseMaterialSolidColor
                ))

                if material.DiffuseMaterialImageSource != "":
                    localImageFile = material.DiffuseMaterialImageSource.replace('\\', '/').split('/').pop()
                    output = self.__outputpath + '/' + localImageFile
                    
                    # copy texture image to output path
                    try:
                        copyfile(material.DiffuseMaterialImageSource, output)
                        pass
                    except IOError as e:
                        print("Unable to copy file. %s" % e)
                        pass
                
                    # write texture xaml
                    file.write("""
                    <DiffuseMaterial>
                        <DiffuseMaterial.Brush>
                            <ImageBrush ImageSource="%s" ViewportUnits="Absolute" TileMode="Tile" />
                        </DiffuseMaterial.Brush>
                    </DiffuseMaterial>""" % (
                localImageFile
                ))

                if material.SpecularColor != "":
                    file.write("""
                    <SpecularMaterial SpecularPower="%s">
                        <SpecularMaterial.Brush>
                            <SolidColorBrush Color="%s" />
                        </SpecularMaterial.Brush>
                    </SpecularMaterial>""" % (
                material.SpecularPower, material.SpecularColor
                ))

                if material.Emit:
                    file.write("""
                    <EmissiveMaterial Color="Transparent" Brush="White" />""")
                
                file.write("""
                </MaterialGroup>""")

            # in case some elements have no material configured, create a
            # default material ourselves
            if self.CreateSomeMaterial:
                file.write("""
                <MaterialGroup x:Key="SomeMaterial">
                    <DiffuseMaterial>
                        <DiffuseMaterial.Brush>
                            <SolidColorBrush Color="#ffffff"/>
                        </DiffuseMaterial.Brush>
                    </DiffuseMaterial>
                </MaterialGroup>""")

            file.write("""
            </Viewport3D.Resources>
            <Viewport3D.Camera>
                <StaticResource ResourceKey="%s" />
            </Viewport3D.Camera>""" % (
        self.ActiveCamera.Name
        ))

            file.write("""
            <ModelVisual3D>""")

            # write elements
            r = 0
            for element in self.Elements:
                if not element.Positions:
                    continue

                file.write("""
                <!-- %s --><ModelVisual3D x:Name="%s" AutomationProperties.AutomationId="%s">""" % (
                r, element.Name, element.Name
                ))
                r += 1

                for subElement in element.Positions:
                    materialProperty = ""
                    if element.Material[subElement] != None:
                        materialProperty = """Material="{StaticResource %s}" """ % (
                                            element.Material[subElement].Name)
                    backMaterialProperty = ""
                    if element.BackMaterial[subElement] != None:
                        backMaterialProperty = """BackMaterial="{StaticResource %s}" """ % (
                                            element.BackMaterial[subElement].Name)

                    textureCoordinatesProperty = ""
                    if element.TextureCoordinates[subElement] != "":
                        textureCoordinatesProperty = """TextureCoordinates="%s" """ % (
                                        element.TextureCoordinates[subElement])

                    file.write("""
                    <ModelVisual3D>
                        <ModelVisual3D.Content>
                            <GeometryModel3D %s%s>
                                <GeometryModel3D.Geometry>
                                    <MeshGeometry3D Positions="%s"
                                                    TriangleIndices="%s"
                                                    Normals="%s"
                                                    %s/>
                                </GeometryModel3D.Geometry>
                            </GeometryModel3D>
                        </ModelVisual3D.Content>""" % (
                    materialProperty, backMaterialProperty,
                    element.Positions[subElement],
                    element.TriangleIndices[subElement],
                    element.Normals[subElement],
                    textureCoordinatesProperty
                    ))

                    if element.CanBeHidden:
                        if element.Owner.hide or element.Owner.hide_render:
                            value = 0
                        else:
                            value = 1
                        file.write("""
                        <ModelVisual3D.Transform>
                            <ScaleTransform3D ScaleX="%s" ScaleY="%s" ScaleZ="%s" />
                        </ModelVisual3D.Transform>""" % (
                        value, value, value
                        ))

                    file.write("""
                    </ModelVisual3D>""")

                if element.Animated:
                    center = element.CenterPosition()
                    scalecenter = element.Owner.matrix_world.decompose()[0]
                    file.write("""
                    <ModelVisual3D.Transform>
                        <Transform3DGroup>
                            <TranslateTransform3D />
                            <RotateTransform3D CenterX="%s" CenterY="%s"  CenterZ="%s">
                                <RotateTransform3D.Rotation>
                                    <QuaternionRotation3D />
                                </RotateTransform3D.Rotation>
                            </RotateTransform3D>
                            <ScaleTransform3D CenterX="%s" CenterY="%s"  CenterZ="%s"/>
                        </Transform3DGroup>
                    </ModelVisual3D.Transform>""" % (
                    round(center.x, 5), round(center.y, 5),
                    round(center.z, 5),
                    round(scalecenter.x, 5), round(scalecenter.y, 5),
                    round(scalecenter.z, 5)
                    ))

                file.write("""
                </ModelVisual3D>""")

            # write lights
            for light in self.Lights:
                if light.Type == "PointLight":
                    file.write("""
                    <ModelVisual3D x:Name="%s">""" % (
                    light.Name
                    ))
                    for i in range(0, 1 + int(light.Energy / 20)):
                        file.write("""
                        <ModelVisual3D> <!-- %s -->
                            <ModelVisual3D.Content>
                                <PointLight Color="%s" Position="%s,%s,%s" LinearAttenuation="%s" />
                            </ModelVisual3D.Content>
                        </ModelVisual3D>""" % (
                        light.Energy,
                        light.Color,
                        round(light.Position.x, 5), round(light.Position.y, 5),
                        round(light.Position.z, 5),
                        light.LinearAttenuation
                        ))
                    file.write("""
                        <ModelVisual3D.Transform>
                            <Transform3DGroup>
                                <TranslateTransform3D />
                            </Transform3DGroup>
                        </ModelVisual3D.Transform>
                    </ModelVisual3D>""")
                else:
                    file.write("""
                    <ModelVisual3D x:Name="%s">
                        <ModelVisual3D.Content>""" % (
                    light.Name
                    ))
                    if light.Type == "DirectionalLight":
                        file.write("""
                            <DirectionalLight Color="%s" Direction="%s,%s,%s" />""" % (
                        light.Color,
                        round(light.Position.x, 5), round(light.Position.y, 5),
                        round(light.Position.z, 5),
                        ))
                    elif light.Type == "AmbientLight":
                        file.write("""
                            <AmbientLight Color="%s" />""" % (
                        light.Color
                        ))
                    file.write("""
                        </ModelVisual3D.Content>
                    </ModelVisual3D>""")

            file.write("""
            </ModelVisual3D>""")

            concurrentStrips = {}
            for animationTrackName in self.AnimationTracks:
                animationTrack = self.AnimationTracks[animationTrackName]
                for nla1 in animationTrack.NLATracks:
                    for strip in nla1.AnimationStrips:
                        if len(nla1.AnimationStrips) == 0:
                            continue
                        if strip not in concurrentStrips:
                            concurrentStrips[strip] = [];
                        for nla2 in animationTrack.NLATracks:
                            for otherStrip in list(nla2.AnimationStrips.keys()):
                                if otherStrip == strip:
                                    continue
                                if otherStrip not in concurrentStrips:
                                    concurrentStrips[otherStrip] = [];
                                strip1 = nla1.AnimationStrips[strip]
                                strip2 = nla2.AnimationStrips[otherStrip]
                                if otherStrip in concurrentStrips[strip]:
                                    continue
                                if self.ConcurrentAnimationStrips(strip1, strip2):
                                    concurrentStrips[strip].append(otherStrip)
                                    concurrentStrips[otherStrip].append(strip)


            # write animation property triggers
            viewport3DStyle = False
            self.SortedAnimations = []
            concurrentStripNames = {}
            nlaTracks = list(self.AnimationTracks[c].NLATracks for c in self.AnimationTracks)
            for animationName in sorted(self.AnimationTracks):
                animation = self.AnimationTracks[animationName]
                if animation.Trigger:
                    for nla in animation.NLATracks:
                        for strip in list(nla.AnimationStrips.keys()):
                            privateName = strip[0].lower() + strip[1:]

                            currentStripAnimation = next((anim for anim in self.SortedAnimations if anim[0] == strip), None)
                            if currentStripAnimation == None:
                                currentStripAnimation = [strip, privateName, []]
                                self.SortedAnimations.append(currentStripAnimation)
                            currentStripAnimation[2] += concurrentStrips[strip]
                            currentStripAnimation[2] = list(set(currentStripAnimation[2]))

            self.SortedAnimations = sorted(self.SortedAnimations)

            if "MyStoryboard" in storyboardAnimations:
                # write animation event triggers (auto start)
                if not self.__usePropertyTriggers:
                    file.write("""
            <Viewport3D.Triggers>
                <EventTrigger RoutedEvent="Viewport3D.Loaded">
                    <BeginStoryboard Storyboard="{StaticResource MyStoryboard}" />
                </EventTrigger>
            </Viewport3D.Triggers>""")

            file.write("""
        </Viewport3D>
    </Border>
    <!-- End Blender content -->
</Page>""")

        finally:
            file.close()

    def WriteCSharp(self, useWPF):
        csfile = None
        try:
            csfile = open(self.__csfilepath, 'w')
            csfile.write("""using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Media3D;

namespace System
{
    #region Blender generated content
    public partial class Animator
    {
        private System.Windows.Controls.Page xamlRoot = null;
        private System.Windows.Controls.Border xamlBorder = null;
        private System.Windows.Controls.Viewport3D xamlObject = null;""")
            if not useWPF:
                csfile.write("""
        private Windows.Forms.Integration.ElementHost elementHost = null;
""")
            else:
                csfile.write("""
        private System.Windows.Controls.Panel panel = null;
""")
        
            if not xamlFile.UseFixedCameraViews:
                csfile.write("""        private double cameraHorizontalAngle = 0;
        private double cameraZoomFactor = 0;
        private double absoluteInitialCameraPositionY = 0;
        private double initialCameraHorizontalAngle = Math.PI;
        private double initialCameraPositionY = 0;
        private double initialMousePositionX = 0;
        private double initialMousePositionY = 0;
        private bool fixedCamera { get; set; }
        private double _maximumZoom = Int32.MaxValue;
        private double _minimumZoom = 1;
        
        public event MouseEventHandler MouseDown;
        public event MouseEventHandler MouseUp;
""")
            action = ""
            for animation in self.SortedAnimations:
                if action == animation[0]:
                    continue
                action = animation[0]
                privateName = animation[1]
                csfile.write("""        public Element %s { get; set; }
""" % (action))

            csfile.write("""
        /// <summary>
        /// This method will calculate which mesh has been clicked on.
        /// It will return the selected mesh
        /// </summary>
        /// <param name="viewport3D">The viewport to look in to</param>
        /// <param name="mousePosition">The mouse position when clicked</param>
        /// <returns>The selected mesh; null if nothing selected</returns>
        private ModelVisual3D raycast(System.Windows.Controls.Viewport3D viewport3D, Point mousePosition)
        {
            PointHitTestParameters hitParams = new PointHitTestParameters(mousePosition);
            RayMeshGeometry3DHitTestResult selectedMesh = VisualTreeHelper.HitTest(viewport3D, mousePosition) as RayMeshGeometry3DHitTestResult;
            if (selectedMesh != null)
            {
                return (viewport3D.Children[0] as ModelVisual3D).Children.OfType<ModelVisual3D>().FirstOrDefault(c => (c.Children[0] as ModelVisual3D).Content == selectedMesh.ModelHit);
            }

            return null;
        }

        /// <summary>
        /// This method is fired when a mouse button is pushed down
        /// It will fire the MouseDown event with the clicked mesh in its MouseEventArgs
        /// </summary>
        /// <param name="sender">The XAML object</param>
        /// <param name="e">the additional mousedown event arguments</param>
        private void xamlObject_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (MouseDown == null) return;

            System.Windows.Controls.Viewport3D viewport3D = sender as System.Windows.Controls.Viewport3D;
            Point mousePosition = e.GetPosition(viewport3D);
            ModelVisual3D model3D = raycast(viewport3D, mousePosition);
            if (model3D != null)
            {
                MouseButtonEventArgs me = new MouseButtonEventArgs(e.MouseDevice, e.Timestamp, e.ChangedButton);
                me.RoutedEvent = System.Windows.Controls.Button.ClickEvent;
                me.Source = model3D;
                MouseDown(model3D, me);
            }
        }

        /// <summary>
        /// This method is fired when a mouse button is released
        /// It will fire the MouseUp event with the clicked mesh in its MouseEventArgs
        /// </summary>
        /// <param name="sender">The XAML object</param>
        /// <param name="e">the additional mouseup event arguments</param>
        private void xamlObject_MouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (MouseUp == null) return;

            System.Windows.Controls.Viewport3D viewport3D = sender as System.Windows.Controls.Viewport3D;
            Point mousePosition = e.GetPosition(viewport3D);
            ModelVisual3D model3D = raycast(viewport3D, mousePosition);
            if (model3D != null)
            {
                MouseButtonEventArgs me = new MouseButtonEventArgs(e.MouseDevice, e.Timestamp, e.ChangedButton);
                me.RoutedEvent = System.Windows.Controls.Button.ClickEvent;
                me.Source = model3D;
                MouseUp(model3D, me);
            }
        }

        /// <summary>
        /// gets or sets the canvas background color
        /// </summary>
        public Brush Background
        {
            get
            {
                if (xamlBorder == null)
                    return SystemColors.ControlBrush;
                else
                    return xamlBorder.Background;
            }

            set
            {
                if (xamlBorder != null)
                {
                    xamlBorder.Background = value;
                }
            }
        }

        /// <summary>
        /// Represents the active camera
        /// </summary>
        public PerspectiveCamera Camera
        {
            get
            {
                return xamlObject.Camera as PerspectiveCamera;
            }
        }

        /// <summary>
        /// Minimum camera zoom level
        /// </summary>
        public double MaximumZoom
        {
            get
            {
                return _maximumZoom;
            }

            set
            {
                if (value > _minimumZoom)
                {
                    _maximumZoom = value;
                }
            }
        }

        /// <summary>
        /// Maximum camera zoom level
        /// </summary>
        public double MinimumZoom
        {
            get
            {
                return _minimumZoom;
            }

            set
            {
                if (value >= 1)
                {
                    _minimumZoom = value;

                    if (_maximumZoom < _minimumZoom)
                    {
                        _maximumZoom = Int32.MaxValue;
                    }
                }
            }
        }

        /// <summary>
        /// Gets a collection of all available meshes
        /// </summary>
        public IEnumerable<ModelVisual3D> Meshes
        {
            get
            {
                return (xamlObject.Children[0] as ModelVisual3D).Children.OfType<ModelVisual3D>().Where(m => string.IsNullOrWhiteSpace(m.Identifier()) == false);
            }
        }
""")
            if useWPF:
                csfile.write("""        
        /// <summary>
        /// Animator Constructor
        /// </summary>
        /// <param name="panel">The Panel element in which the XAML content should be shown</param>
        /// <param name="fixedCamera">If set to true, the camera view cannot be modified by means of the mouse</param>
        public Animator(System.Windows.Controls.Panel panel, bool fixedCamera = false)
        {
            this.xamlBorder = null;
            this.panel = panel;
            panel.SizeChanged += new SizeChangedEventHandler(panel_SizeChanged);""")
                if not xamlFile.UseFixedCameraViews:
                    csfile.write("""
            this.fixedCamera = fixedCamera;""")
                csfile.write("""
        }
""")
            else:
                csfile.write("""
        /// <summary>
        /// Animator Constructor
        /// </summary>
        /// <param name="elementHost">The ElementHost element in which the XAML content should be shown</param>
        /// <param name="fixedCamera">If set to true, the camera view cannot be modified by means of the mouse</param>
        public Animator(System.Windows.Forms.Integration.ElementHost elementHost, bool fixedCamera = false)
        {
            this.xamlBorder = null;
            this.elementHost = elementHost;""")
                if not xamlFile.UseFixedCameraViews:
                    csfile.write("""
            this.fixedCamera = fixedCamera;""")
                csfile.write("""
        }
""")
        
            if not xamlFile.UseFixedCameraViews:
                csfile.write("""
        /// <summary>
        /// Animator Destructor
        /// </summary>
        ~Animator()
        {
            if (xamlBorder != null)
            {""")
            
                if xamlFile.UseFixedCameraViews == False:
                    csfile.write("""
                xamlBorder.MouseDown -= new MouseButtonEventHandler(xamlBorder_MouseDown);
                xamlBorder.MouseMove -= new System.Windows.Input.MouseEventHandler(xamlBorder_MouseMove);
                xamlBorder.MouseWheel -= new MouseWheelEventHandler(xamlBorder_MouseWheel);""")
        
                csfile.write("""
                xamlObject.MouseDown -= new System.Windows.Input.MouseButtonEventHandler(xamlObject_MouseDown);
                xamlObject.MouseUp -= new System.Windows.Input.MouseButtonEventHandler(xamlObject_MouseUp);
            }
        }
""")

            if not xamlFile.UseFixedCameraViews:
                csfile.write("""        
        /// <summary>
        /// This method is fired when the mousedown event is fired on the XAML object.
        /// </summary>
        /// <param name="sender">the XAML object</param>
        /// <param name="e">the additional mousedown event arguments</param>
        private void xamlBorder_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (fixedCamera) return;

            if (e.LeftButton == MouseButtonState.Pressed &&
                e.MiddleButton == MouseButtonState.Released &&
                e.RightButton == MouseButtonState.Released)
            {
                initialMousePositionX = e.GetPosition(xamlBorder as IInputElement).X;
                initialMousePositionY = e.GetPosition(xamlBorder as IInputElement).Y / (xamlBorder.ActualHeight / cameraZoomFactor);
                initialCameraHorizontalAngle = cameraHorizontalAngle;
                initialCameraPositionY = Camera.Position.Y - cameraZoomFactor * Math.Cos(cameraHorizontalAngle);
            }
        }

        /// <summary>
        /// This method is fired when the mousemove event is fired on the XAML object
        /// </summary>
        /// <param name="sender">the XAML object</param>
        /// <param name="e">the additional mousemove event arguments</param>
        private void xamlBorder_MouseMove(object sender, System.Windows.Input.MouseEventArgs e)
        {
            if (fixedCamera) return;

            if (e.LeftButton == MouseButtonState.Pressed &&
                e.MiddleButton == MouseButtonState.Released &&
                e.RightButton == MouseButtonState.Released)
            {
                RepaintAfterMouseAction(e);
            }
        }

        /// <summary>
        /// This method is fired when the mousewheel event is fired on the XAML object
        /// </summary>
        /// <param name="sender">the XAML object</param>
        /// <param name="e">the additional mousewheel event arguments</param>
        private void xamlBorder_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            if (fixedCamera) return;

            initialMousePositionX = e.GetPosition(xamlBorder as IInputElement).X;
            initialMousePositionY = e.GetPosition(xamlBorder as IInputElement).Y / 100;
            initialCameraHorizontalAngle = cameraHorizontalAngle;
            initialCameraPositionY = Camera.Position.Y - cameraZoomFactor * Math.Cos(cameraHorizontalAngle);

            if (e.Delta < 0)
                cameraZoomFactor++;
            else
                cameraZoomFactor--;

            if (cameraZoomFactor < _minimumZoom)
            {
                cameraZoomFactor = _minimumZoom;
            }

            if (cameraZoomFactor > _maximumZoom)
            {
                cameraZoomFactor = _maximumZoom;
            }

            RepaintAfterMouseAction(e);
        }

        /// <summary>
        /// Redraws the XAML object as seen from the camera position
        /// This internal method is fired after some mouse events occur on the XAML object
        /// </summary>
        /// <param name="e">The given mouse event</param>
        private void RepaintAfterMouseAction(MouseEventArgs e)
        {
            double mouseDeltaPositionY;
            if (e == null)
            {
                mouseDeltaPositionY = 0;
                absoluteInitialCameraPositionY = Camera.Position.Z;
                RotateTransform3D cameraRotateTransform3D = Camera.Transform as RotateTransform3D;
                if (cameraRotateTransform3D != null)
                {
                    QuaternionRotation3D cameraQuaternionRotation3D = cameraRotateTransform3D.Rotation as QuaternionRotation3D;
                    if (cameraQuaternionRotation3D != null)
                    {
                        Point3D euler = new Point3D();
                        euler.X = Math.Asin(2 * (cameraQuaternionRotation3D.Quaternion.X * cameraQuaternionRotation3D.Quaternion.Z - cameraQuaternionRotation3D.Quaternion.W * cameraQuaternionRotation3D.Quaternion.Y));
                        euler.Y = Math.Atan2((2 * cameraQuaternionRotation3D.Quaternion.X * cameraQuaternionRotation3D.Quaternion.W) + 
                                             (2 * cameraQuaternionRotation3D.Quaternion.Y * cameraQuaternionRotation3D.Quaternion.Z), 
                                             1 - 2 * (Math.Pow(cameraQuaternionRotation3D.Quaternion.Z, 2) + Math.Pow(cameraQuaternionRotation3D.Quaternion.W, 2)));
                        euler.Z = Math.Asin((2 * cameraQuaternionRotation3D.Quaternion.X * cameraQuaternionRotation3D.Quaternion.Y) + 
                                             (2 * cameraQuaternionRotation3D.Quaternion.Z * cameraQuaternionRotation3D.Quaternion.W));
                        cameraHorizontalAngle = Math.PI - Math.Asin(euler.Z);
                        cameraZoomFactor = -Camera.Position.Y;
                    }
                }
                else
                {
                    cameraZoomFactor = Camera.Position.Z + (Math.Abs(Camera.Position.X) + Math.Abs(Camera.Position.Y)) / 2;
                }
            }
            else
            {
                Point mousePosition = e.GetPosition(xamlBorder as IInputElement);
                cameraHorizontalAngle = initialCameraHorizontalAngle + ((mousePosition.X - initialMousePositionX) * 2 * Math.PI) / xamlBorder.ActualWidth;
                if (e is MouseWheelEventArgs)
                    mouseDeltaPositionY = mousePosition.Y / 100 - initialMousePositionY;
                else
                    mouseDeltaPositionY = mousePosition.Y / (xamlBorder.ActualHeight / cameraZoomFactor) - initialMousePositionY;
            }

            double w = Math.Sin(cameraHorizontalAngle / 2) * Math.Sqrt(0.5);
            double y = Math.Cos(cameraHorizontalAngle / 2) * Math.Sqrt(0.5);
            double x = Math.Sin(cameraHorizontalAngle / 2) * Math.Sqrt(0.5);
            double z = Math.Cos(cameraHorizontalAngle / 2) * Math.Sqrt(0.5);
            Quaternion quaternion = new Quaternion(x, y, z, w);
            Point3D center = new Point3D(cameraZoomFactor * Math.Sin(cameraHorizontalAngle), cameraZoomFactor * Math.Cos(cameraHorizontalAngle), 0);

            Transform3DGroup cameraTransform3DGroup = new Transform3DGroup();
            cameraTransform3DGroup.Children.Add(new RotateTransform3D(new QuaternionRotation3D(quaternion), center));
            cameraTransform3DGroup.Children.Add(new TranslateTransform3D(0, 0, absoluteInitialCameraPositionY));
            Camera.Transform = cameraTransform3DGroup;
            Camera.Position = new Point3D(cameraZoomFactor * Math.Sin(cameraHorizontalAngle), initialCameraPositionY + mouseDeltaPositionY + cameraZoomFactor * Math.Cos(cameraHorizontalAngle), 0);
        }""")
            
            csfile.write("""
        
        /// <summary>
        /// Loads a loose XAML file with a Page root element.
        /// The XAML content will be shown in the Animators Canvas
        /// </summary>
        /// <param name="xamlFileName">The XAML file to be loaded</param>
        /// <param name="resourcesSubdirectory">Relative subdirectory where texture images are stored in</param>
        public void LoadXamlFile(string xamlFileName, string resourcesSubdirectory = ".")
        {""")
        
            if not xamlFile.UseFixedCameraViews:
                csfile.write("""
            if (xamlBorder != null)
            {
                xamlBorder.MouseDown -= new MouseButtonEventHandler(xamlBorder_MouseDown);
                xamlBorder.MouseMove -= new System.Windows.Input.MouseEventHandler(xamlBorder_MouseMove);
                xamlBorder.MouseWheel -= new MouseWheelEventHandler(xamlBorder_MouseWheel);
            }""")
            
            csfile.write("""            
            using (Stream sr = File.OpenRead(xamlFileName))
            {
                var applicationDirectory = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
                var runtimeResourcesDirectory = Path.Combine(applicationDirectory, resourcesSubdirectory);
                var pc = new ParserContext
                {
                    BaseUri = new Uri(runtimeResourcesDirectory, UriKind.Absolute)
                };

                xamlRoot = XamlReader.Load(sr, pc) as System.Windows.Controls.Page;
                if (xamlRoot != null)
                {
                    xamlBorder = xamlRoot.Content as System.Windows.Controls.Border;""")
            
            if not xamlFile.UseFixedCameraViews:
                csfile.write("""
                    xamlBorder.MouseDown += new MouseButtonEventHandler(xamlBorder_MouseDown);
                    xamlBorder.MouseMove += new System.Windows.Input.MouseEventHandler(xamlBorder_MouseMove);
                    xamlBorder.MouseWheel += new MouseWheelEventHandler(xamlBorder_MouseWheel);""")
            
            csfile.write("""                    
                    xamlObject = xamlBorder.Child as System.Windows.Controls.Viewport3D;
                    xamlRoot.Content = null;
""") 

            if useWPF:
                csfile.write("""
                    if (panel != null)
                    {
                        panel.Children.Clear();
                        panel.Children.Add(xamlBorder);
                    }
""")
            else:
                csfile.write("""
                    if (elementHost != null)
                    {
                        elementHost.Child = xamlBorder;
                    }
""")

            csfile.write("""
                    xamlObject.DataContext = this;""")

            action = ""
            for animation in self.SortedAnimations:
                if action == animation[0]:
                    continue
                action = animation[0]

                csfile.write("""
                    %s = new Element(this, xamlObject.Resources["storyboard%s"] as Storyboard);""" % (
                    action, action))
        
            action = ""
            for animation in self.SortedAnimations:
                if action == animation[0]:
                    continue
                action = animation[0]
                
                if len(animation[2]) > 0:
                    csfile.write("""
                    %s.concurrentElements = new Element[] {""" % (
                    action))
                    
                    for index in range(0, len(animation[2]) - 1):
                        csfile.write("""
                                                            %s,""" % (
                                    animation[2][index]))
                 
                    csfile.write("""
                                                            %s
                                                          };
""" % (
                                    animation[2][len(animation[2]) - 1]))

            csfile.write("""
                    xamlObject.MouseDown += new System.Windows.Input.MouseButtonEventHandler(xamlObject_MouseDown);
                    xamlObject.MouseUp += new System.Windows.Input.MouseButtonEventHandler(xamlObject_MouseUp);
                }
            }""")
            
            if not xamlFile.UseFixedCameraViews:
                csfile.write("""
            
            if (fixedCamera == false)
            {
                RepaintAfterMouseAction(null);
            }""")
            
            csfile.write("""
        }

        void panel_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            xamlBorder.Width = e.NewSize.Width;
            xamlBorder.Height = e.NewSize.Height;
        }
        
        public class Element
        {
            public enum State
            {
                Inexistant,
                NotRunning,
                Running,
                Finished
            }

            private State _active;
            private Animator _animator;
            private Storyboard _reverseStoryboard;
            private Storyboard _storyboard;
            
            internal Element[] concurrentElements { get; set; }

            /// <summary>
            /// The state of the storyboard element
            /// </summary>
            internal State Active
            {
                get
                {
                    return _active;
                }
            }

            /// <summary>
            /// Constructor for a storyboard element
            /// </summary>
            /// <param name="animator">The animator owner class</param>
            /// <param name="storyboard">The storyboard linked to</param>
            /// <param name="concurrentStoryboards">All concurrent storyboard which animations may interfere with one of the given storyboard</param>
            internal Element(Animator animator, Storyboard storyboard)
            {
                _active = State.Inexistant;
                if (storyboard == null) return;

                _active = State.NotRunning;
                _animator = animator;
                _reverseStoryboard = null;

                if (storyboard.IsFrozen)
                {
                    _storyboard = storyboard.Clone();
                }
                else
                {
                    _storyboard = storyboard;
                }

                concurrentElements = new Element[0];

                _storyboard.Completed += new EventHandler(_storyboard_Completed);
            }

            /// <summary>
            /// Returns true if a storyboard element is playing or was the last one playing
            /// </summary>
            /// <param name="value"></param>
            /// <returns></returns>
            public static implicit operator bool(Element value)
            {
                return value.Active == State.Finished ||
                       value.Active == State.Running;
            }

            /// <summary>
            /// This method is fired when a storyboard has finished playing
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            void _storyboard_Completed(object sender, EventArgs e)
            {
                if (_active == State.Running)
                {
                    _active = State.Finished;
                }
            }

            /// <summary>
            /// This method is fired when a storyboard has finished reversed playing
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            void _storyboard_Reversed(object sender, EventArgs e)
            {
                if (_active == State.Running)
                {
                    _active = State.NotRunning;
                }
            }

            /// <summary>
            /// Calculates the beginning and ending frames of a given storyboard and
            /// converts these values to a BeginTime TimeSpan and a Duration
            /// </summary>
            /// <param name="storyboard">The given storyboard</param>
            /// <param name="pctBegin">Frame percentage from where the calculation should start</param>
            /// <param name="pctEnd">Frame percentage to where the calculation should stop</param>
            /// <param name="beginTime"></param>
            /// <param name="duration"></param>
            private void CalculateStoryboardKeyFrames(Storyboard storyboard, float pctBegin, float pctEnd, out TimeSpan beginTime, out Duration duration)
            {
                if (pctBegin < 0) pctBegin = 0;
                if (pctBegin > 100) pctBegin = 100;
                if (pctEnd < pctBegin || pctEnd > 100) pctEnd = 100;

                Timeline lastFrame = storyboard.Children.Last();
                TimeSpan endTime = lastFrame.BeginTime.Value.Add(lastFrame.Duration.TimeSpan);

                beginTime = TimeSpan.FromMilliseconds((-endTime.TotalMilliseconds * pctBegin) / 100);
                duration = new Duration(TimeSpan.FromMilliseconds((endTime.TotalMilliseconds * pctEnd) / 100));
            }

            /// <summary>
            /// Plays the storyboard partially
            /// </summary>
            /// <param name="pctBegin">Frame percentage from where the animation should start</param>
            /// <param name="pctEnd">Frame percentage to where the animation should stop</param>
            /// <param name="relocate">True if the location is relative, false if absolute</param>
            internal void Play(float pctBegin, float pctEnd, bool relocate = false)
            {
                if (_active != State.Inexistant)
                {
                    TimeSpan beginTime;
                    Duration duration;
                    Storyboard storyboard;

                    if (pctBegin > pctEnd)
                    {
                        if (_reverseStoryboard == null)
                        {
                            Dictionary<string, Timeline> nextFrame = new Dictionary<string, Timeline>();
                            _reverseStoryboard = new Storyboard();
                            _reverseStoryboard.Completed += new EventHandler(_storyboard_Reversed);
                            double totalDurationMilliseconds = _storyboard.Children.Last<Timeline>().BeginTime.Value.TotalMilliseconds + 
                                                               _storyboard.Children.Last<Timeline>().Duration.TimeSpan.TotalMilliseconds;
                            for (int childIndex = _storyboard.Children.Count - 1; childIndex > 0; childIndex--)
                            {
                                Timeline animation = _storyboard.Children[childIndex];
                                Timeline newAnimation = animation.Clone();
                                string targetProperty = Storyboard.GetTargetProperty(animation).Path;
                                Timeline lastFrame;
                                if (!nextFrame.TryGetValue(targetProperty, out lastFrame))
                                {
                                    newAnimation.BeginTime = new TimeSpan?(TimeSpan.FromMilliseconds(0.0));
                                    newAnimation.Duration = new Duration(TimeSpan.FromMilliseconds(0.0));
                                }
                                else
                                {
                                    double beginMilliseconds = totalDurationMilliseconds - (lastFrame.BeginTime.Value.TotalMilliseconds + lastFrame.Duration.TimeSpan.TotalMilliseconds);
                                    if (beginMilliseconds == 0.0)
                                    {
                                        beginMilliseconds = 10.0;
                                    }
                                    newAnimation.BeginTime = new TimeSpan?(TimeSpan.FromMilliseconds(beginMilliseconds));
                                    newAnimation.Duration = lastFrame.Duration;
                                }
                                nextFrame[targetProperty] = animation;
                                _reverseStoryboard.Children.Add(newAnimation);
                            }
                        }
                        storyboard = _reverseStoryboard;
                        CalculateStoryboardKeyFrames(storyboard, 100f - pctBegin, 100f - pctEnd, out beginTime, out duration);
                    }
                    else
                    {
                        storyboard = _storyboard;
                        CalculateStoryboardKeyFrames(storyboard, pctBegin, pctEnd, out beginTime, out duration);
                    }

                    _active = State.Running;

                    storyboard.BeginTime = beginTime;
                    storyboard.Duration = duration;

                    foreach (Element concurrent in concurrentElements)
                    {
                        concurrent.Stop();
                    }

                    if (relocate)
                    {
                        var storyboardChildInitiators = storyboard.Children.OfType<DoubleAnimation>().GroupBy(c => Storyboard.GetTargetProperty(c).Path).Select(g => g.First()).ToDictionary(k => Storyboard.GetTargetProperty(k).Path, v => v.To);
                        foreach (var storyboardChildInitiator in storyboardChildInitiators)
                        {
                            string[] targetPropertyItems = storyboardChildInitiator.Key.Split('.');
                            if (targetPropertyItems.Length == 5 &&
                                targetPropertyItems[3] == "Children[0]")
                            {
                                int childIndex = Int32.Parse(targetPropertyItems[1].Split('[', ']')[1]);
                                ModelVisual3D mesh = (_animator.xamlObject.Children[0] as ModelVisual3D).Children[childIndex] as ModelVisual3D;
                                TranslateTransform3D relocationOffset = (mesh.Transform as Transform3DGroup).Children[0] as TranslateTransform3D;
                                TranslateTransform3D globalOffset = (mesh.Children[0] as ModelVisual3D).Transform as TranslateTransform3D;
                                if (globalOffset == null)
                                {
                                    globalOffset = new TranslateTransform3D(0, 0, 0);
                                }

                                Point3D relocation = new Point3D(relocationOffset.OffsetX, relocationOffset.OffsetY, relocationOffset.OffsetZ);

                                switch (targetPropertyItems[4])
                                {
                                    case "OffsetX":
                                        relocation.X -= storyboardChildInitiator.Value.Value - globalOffset.OffsetX;
                                        relocation.Y += globalOffset.OffsetY;
                                        relocation.Z += globalOffset.OffsetZ;
                                        break;
                                    case "OffsetY":
                                        relocation.X += globalOffset.OffsetX;
                                        relocation.Y -= storyboardChildInitiator.Value.Value - globalOffset.OffsetY;
                                        relocation.Z += globalOffset.OffsetZ;
                                        break;
                                    case "OffsetZ":
                                        relocation.X += globalOffset.OffsetX;
                                        relocation.Y += globalOffset.OffsetY;
                                        relocation.Z -= storyboardChildInitiator.Value.Value - globalOffset.OffsetZ;
                                        break;
                                }
                                (mesh.Children[0] as ModelVisual3D).Transform = new TranslateTransform3D(relocation.X, relocation.Y, relocation.Z);
                                (mesh.Transform as Transform3DGroup).Children[0] = new TranslateTransform3D(0, 0, 0);
                            }
                        }
                    }
                    
                    storyboard.Begin(_animator.xamlObject, true);
                }
            }
            
            /// <summary>
            /// Plays the storyboard animation completely
            /// </summary>
            /// <param name="relocate">True if the location is relative, false if absolute</param>
            internal void Play(bool relocate = false)
            {
                Play(0, 100, relocate);
            }

            /// <summary>
            /// Sets the storyboard animation to a specific frame
            /// </summary>
            /// <param name="pct">The frame that should be shown [percentage]</param>
            internal void Play(float pct)
            {
                if (_active != State.Inexistant)
                {
                    TimeSpan beginTime;
                    Duration duration;

                    CalculateStoryboardKeyFrames(_storyboard, pct, pct, out beginTime, out duration);

                    _active = State.Running;

                    _storyboard.BeginTime = beginTime;
                    _storyboard.Duration = new Duration(-beginTime);

                    foreach (Element concurrent in concurrentElements)
                    {
                        concurrent.Stop();
                    }

                    _storyboard.Begin(_animator.xamlObject, true);

                    _active = State.NotRunning;
                }
            }

            /// <summary>
            /// Plays the storyboard animation backwards
            /// </summary>
            /// <param name="relocate">True if the location is relative, false if absolute</param>
            internal void Reverse(bool relocate = false)
            {
                Play(100, 0, relocate);
            }

            /// <summary>
            /// Stops the storyboard element
            /// </summary>
            internal void Stop()
            {
                if (_active != State.Inexistant)
                {
                    _active = State.NotRunning;
                    _storyboard.Stop(_animator.xamlObject);
                }
            }
        }
    }

    public static class ModelVisual3DExtension
    {
        /// <summary>
        /// Returns the geometry of the mesh
        /// </summary>
        /// <param name="modelVisual3D">The current mesh</param>
        /// <returns>The geometry of the mesh</returns>
        public static MeshGeometry3D Geometry(this ModelVisual3D modelVisual3D)
        {
            if (modelVisual3D.Children.Count > 0)
            {
                ModelVisual3D modelVisual3DChild = modelVisual3D.Children.First() as ModelVisual3D;
                if (modelVisual3DChild != null)
                {
                    GeometryModel3D geometryModel3D = modelVisual3DChild.Content as GeometryModel3D;
                    if (geometryModel3D != null)
                    {
                        MeshGeometry3D meshGeometry3D = geometryModel3D.Geometry as MeshGeometry3D;
                        return meshGeometry3D;
                    }
                }
            }
            return null;
        }
        
        /// <summary>
        /// Returns the name of the mesh
        /// </summary>
        /// <param name="modelVisual3D">The current mesh</param>
        /// <returns>The name of the mesh</returns>
        public static string Identifier(this ModelVisual3D modelVisual3D)
        {
            return modelVisual3D.GetValue(System.Windows.Automation.AutomationProperties.AutomationIdProperty).ToString();
        }

        /// <summary>
        /// Returns all linked materials
        /// </summary>
        /// <param name="modelVisual3D">The current mesh</param>
        /// <returns>A collection of materials</returns>
        public static IEnumerable<Material> Materials(this ModelVisual3D modelVisual3D)
        {
            if (modelVisual3D.Children.Count <= 0) return new List<Material>();
            ModelVisual3D modelVisual3DChild = modelVisual3D.Children.First() as ModelVisual3D;

            GeometryModel3D modelVisual3DContent = modelVisual3DChild.Content as GeometryModel3D;
            if (modelVisual3DContent == null) return new List<Material>();

            if ((modelVisual3DContent.Material as MaterialGroup) == null) return new List<Material>();

            return (modelVisual3DContent.Material as MaterialGroup).Children.OfType<Material>();
        }
    }
    #endregion Blender generated content
}""")
        finally:
            csfile.close()

###############################################################################
class XamlExporter(bpy.types.Operator):
    """Xaml Export Script"""           # blender will use this as a tooltip for menu items and buttons.
    bl_idname = "export.xaml"          # unique identifier for buttons and menu items to reference.
    bl_label = "Export Xaml"           # display name in the interface.
    filepath = StringProperty(subtype='FILE_PATH')
    file = None

    def invoke(self, context, event):
        # check if active object is visible
        if (bpy.context.scene.objects.active == None or
            bpy.context.scene.objects.active.hide):
            # active object is hidden -> make another object active
            visibleObjects = list(o for o in bpy.data.objects if not o.hide)
            if not visibleObjects:
                # no visible objects -> error
                self.report({'ERROR'},
                           "There are no visible objects in the current scene")
                return {'CANCELLED'}
            bpy.context.scene.objects.active = visibleObjects[0]

        # generate default name for 'save as' dialog
        if not self.filepath:
            if not bpy.data.filepath:
                filepath = "untitled"
            else:
                filepath = bpy.data.filepath
            self.filepath = os.path.splitext(filepath)[0] + ".xaml"

        # show 'save as' dialog
        context.window_manager.fileselect_add(self)
        return {'RUNNING_MODAL'}
#=============================================================================#
class XamlExporterXamlOnly(XamlExporter):
    """Xaml Export Script"""           # blender will use this as a tooltip for menu items and buttons.
    bl_idname = "export.xaml"          # unique identifier for buttons and menu items to reference.
    bl_label = "Export Xaml"           # display name in the interface.
    filepath = StringProperty(subtype='FILE_PATH')
    file = None

    def execute(self, context):        # execute() is called by blender when running the operator.
        global xamlFile
        global centerPositions
        centerPositions = {}
        XamlFile(self.filepath, False)
        xamlFile.WriteXaml()
        return {'FINISHED'}            # this lets blender know the operator finished successfully.
#=============================================================================#
class XamlExporterXamlWinForms(XamlExporter):
    """Xaml/C# Export Script"""             # blender will use this as a tooltip for menu items and buttons.
    bl_idname = "export.xamlcswinforms"     # unique identifier for buttons and menu items to reference.
    bl_label = "Export Xaml / WinForms C#"  # display name in the interface.
    filepath = StringProperty(subtype='FILE_PATH')
    file = None

    def execute(self, context):        # execute() is called by blender when running the operator.
        global xamlFile
        global centerPositions
        centerPositions = {}
        XamlFile(self.filepath, True)
        xamlFile.WriteXaml()
        xamlFile.WriteCSharp(False)
        return {'FINISHED'}            # this lets blender know the operator finished successfully.
#=============================================================================#
class XamlExporterXamlWPF(XamlExporter):
    """Xaml/C# Export Script"""        # blender will use this as a tooltip for menu items and buttons.
    bl_idname = "export.xamlcswpf"     # unique identifier for buttons and menu items to reference.
    bl_label = "Export Xaml / WPF C#"  # display name in the interface.
    filepath = StringProperty(subtype='FILE_PATH')
    file = None

    def execute(self, context):        # execute() is called by blender when running the operator.
        global xamlFile
        global centerPositions
        centerPositions = {}
        XamlFile(self.filepath, True)
        xamlFile.WriteXaml()
        xamlFile.WriteCSharp(True)
        return {'FINISHED'}            # this lets blender know the operator finished successfully.
#=============================================================================#
def menu_func(self, context):
    self.layout.operator_context = 'INVOKE_DEFAULT'
    self.layout.operator(XamlExporterXamlOnly.bl_idname, text="XAML (.xaml)")
    self.layout.operator(XamlExporterXamlWinForms.bl_idname, text="XAML / WinForms C# (.xaml + .cs)")
    self.layout.operator(XamlExporterXamlWPF.bl_idname, text="XAML / WPF C# (.xaml + .cs)")

def register():
    bpy.utils.register_class(XamlExporterXamlOnly)
    bpy.utils.register_class(XamlExporterXamlWinForms)
    bpy.utils.register_class(XamlExporterXamlWPF)
    bpy.types.INFO_MT_file_export.append(menu_func)

def unregister():
    bpy.utils.unregister_class(XamlExporterXamlOnly)
    bpy.utils.unregister_class(XamlExporterXamlWinForms)
    bpy.utils.unregister_class(XamlExporterXamlWPF)
    bpy.types.INFO_MT_file_export.remove(menu_func)

# This allows you to run the script directly from blenders text editor
# to test the addon without having to install it.
if __name__ == "__main__":
    register()
