# blender_addon_xaml_exporter
Blender addon for exporting to WPF / XAML format

# Purpose
This script exports Blender scene objects into a 3D loose XAML file or into an interactive 3D XAML file combined with a C# class.

# Prerequisites
Blender version 2.78

# Installation
Download the io_export_xaml.py script and copy it into the following directory:

    %appdata%\Blender Foundation\Blender\2.78\scripts

Open Blender and go to the addons tab in User Preferences.  
Enable the script  
Start from File > Export menu.  

# Extra configuration in Blender
WPF seems to use a 5/4 screen size (e.g. 1280x1024).  
To export a correct camera view, you need to add a render preset with the following settings:  
![camera-render-preset-settings.png](https://bitbucket.org/repo/rke5a8/images/2042499047-camera-render-preset-settings.png)

Before exporting the scene to a WPF/Xaml file, you need to select this render preset.  

# Features
## supported data:
    meshes  
    curves  
    fonts  
    lamps  
    camera views  

    Simple animations (e.g. LocRotScale, material diffuse coloring)  


# Animations
Animations should be created in Blender by means of keyframes.  
The following Blender properties will be exported when keyframed:  
* Mesh - Location / Rotation / Scale / Viewport visibility / Render restriction
* Material - Diffuse color / Specular color / Shading emission / Transparency alpha
* Curve - Follow Path

Deforming animations are not supported.

In case one wants to create property based animations, all keyframes need to be converted to NLA strips.  
Each unique NLA strip name will be exported as an Animator Element property.  
These Animator Elements can play their corresponding animations by means of a Play() method.  
An NLA track should not contain multiple NLA strips with the same name.  
NLA strips with the same name can exist over multiple objects.  
An animation track can contain multiple NLA tracks which may contain overlapping NLA strips.  
In case these NLA strips have unique names, these NLA strips will also be converted to a property.  
Be aware that the NLA track which contains most of the animation data should be on top in the NLA Editor.  

When an Animator Element is played, all concurrent animations will be stopped.  
A concurrent animation is an animation existing in the same NLA track where the original animation is located in.  
E.g. if you have an animation for an object named "Move Left" and another for the same object "Move Right", both animations are concurrent animations of each other.  

All animations will be converted to XAML storyboards.  
In case the export type is XAML, the storyboard will be automatically looping.  
In case the export type is XAML/C#, the storyboard will not automatically looping unless the playback settings of the according NLA strip has a Repeat value of more than 1.  


# Export Formats
There are 2 export formats:  
XAML and XAML/C#  

When exporting to XAML, you will generate a loose XAML file.  
In case there are any animations to be exported, the animations will be exported sequentially or as seen as in Blender itself.  

When exporting to XAML/C#, you will generate a loose XAML file and an animator C# class.  
In case there are any animations to be exported, the animations will be exported as Animator Elements.  
These animations will be playing as soon as their corresponding Play method is executed in your C# code.  
The animator C# class should be added to your project.  


# Mouse actions (hit test)
The animator class contains a MouseUp and MouseDown event.
The clicked mesh can be found in the MouseEventArgs parameter of the event:  e.Source

In case the Animator object is instanced with FixedCamera = false, some default mouse events will be generated for navigation purposes,  
e.g. move the camera around the XAML object, zooming in/out, moving the camera up/down.